import React from 'react'

function WishForm(props) {
    let list = props.list.map((wish, index) => {
      return <li key={index}>{wish}</li>
    })  
    return( 
      <div>
        <ul className="ul">{list}</ul>
      </div>
    ) 
}
  
export default WishForm