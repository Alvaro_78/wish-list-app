import React from 'react'

function Input(props){
    return (
        <div>
            <input className="input" 
                placeholder="Escribe Algo..."
                onChange={props.handleInputChange} 
                type="text" 
                value ={props.inputValue}
            />
        </div>
    )
}
export default Input
 