import React, { Component, Fragment } from 'react'
// import DropDown from './components/DropDown'
import Input from './components/Input'
import Button from './components/Button'
import WishForm from './components/WishForm'
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom"

import Info from './components/Info'
import Contact from './components/Contact'


class App extends Component {
  constructor(props) {
    super()
    this.state = {
      wish: [],
      currentWish: "",
      displayMenu: false
    }
    this.showDropdownMenu = this.showDropdownMenu.bind(this)
    this.hideDropdownMenu = this.hideDropdownMenu.bind(this)
  }

  showDropdownMenu(event) {
    event.preventDefault()
    this.setState({ displayMenu: true }, () => {
    document.addEventListener('click', this.hideDropdownMenu)
    })
  }

  hideDropdownMenu() {
    this.setState({ displayMenu: false }, () => {
      document.removeEventListener('click', this.hideDropdownMenu)
    })

  }

  handleButtonSend = (event) => {
    const lastArrayOfState = this.state.wish
    event.preventDefault()
    if(this.state.currentWish!==""){
      this.setState({ wish: lastArrayOfState.concat([this.state.currentWish])})
      this.setState({currentWish:""})

    }
  }

  handleInputChange = (event) => {
    this.setState({currentWish: event.target.value})
  }

  handleButtonNextWish = () => {
    this.setState({wish:[]})
  }


  render(props){
      return (
        <div>
          <Router>
                <div>
                  <ul className="nav">
                    <p className="numLabel">Wishes:{this.state.wish.length}</p>
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/info">Info</Link></li>
                    <li><Link to="/contact">Contact</Link></li>        
                  </ul>
                </div>
              <Switch>
                <Route path="/Fragment" component={Fragment}>
                </Route> 
                <Route path="/Info" component={Info}>
                </Route> 
                <Route path="/Contact" component={Contact}>
                </Route> 
                <Fragment>
                  <div className="divContainer">
                    <Input handleInputChange={this.handleInputChange}  inputValue={this.state.currentWish}/>
                    <Button clickOne={this.handleButtonSend} />
                  </div>
                  <WishForm list={this.state.wish} />
                </Fragment>
              </Switch>
            </Router>   
        </div>
      )
    }
}

export default App